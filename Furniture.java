package furniture;

import java.util.ArrayList;
import java.util.Scanner;

public class Furniture {
    
    int weight, price;
    String name;
    ArrayList<String> color = new ArrayList<String>();
    
    Scanner in = new Scanner(System.in);
    
    public String get_name(){
        
        System.out.println("Enter name of furniture");
        name = in.nextLine();
        return name;
    }
    
    public int get_price(){
        
        System.out.println("Enter price of furniture");
        price = in.nextInt();
        return price;
    }
    
    public int get_weight(){
        
        System.out.println("Enter weight of furniture");
        weight = in.nextInt();
        return weight;  
    }
    
    public void show_furn(){
        System.out.printf("Furniture has this parameters: name - %s, price - %d, weight - %d", name, price, weight);
    }
    
    public void main(String[] args) {
        Furniture f1 = new Furniture();
    }
    
}
